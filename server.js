const express = require('express');
const db = require('./db');


const app = express();

app.use(express.urlencoded({extended: true}));
app.use(express.json());

app.get('/api/customers', async (req, res) => {
  try {
    let results = await db.all();
    res.json(results);
  } catch(e){
    console.log(e);
    res.sendStatus(500);
  }
});

app.post('/api/newcustomer', async (req, res) => {
  try {
    // check children first
    await db.children(req.body.parentID).then(result => {
      console.log("result", result);
      console.log("result", result.length);

      if(result.length < 2){
        // has space then add child
        add(req.body.name,req.body.parentID,req.body.contribution);
        res.json({});
      }else{
        console.log("Trying to add after any child");
        for (var i = 0; i < result.length; i++) {
          let child = result[i];
          if (child.grandchildren <= 1){
            add(req.body.name,child.id,req.body.contribution);
            //db.new(req.body.name,child.id,req.body.contribution);
            break;
          }
        }
        res.json({});
      }
    });
  } catch(e){
    console.log(e);
    res.sendStatus(500);
  }
});

const add = async (name,parentID,contribution) => {
  db.new(name,parentID,contribution).then(result=>{
    console.log("Will get parent for new customer");
    db.one(parentID).then(result => {
      console.log("Will verify cycle for grandparent "+result.parentID);  
      verifyCycle(result.parentID);
    });
  },err=>{
    console.log(err)
  });
}

const verifyCycle = async (id) => {
  console.log("Verifing cycle for "+id);
  await db.children(id).then(result => {
    if(result.length >= 2){
      var grandchildren = 0;
      for (var i = 0; i < result.length; i++) {
        let child = result[i];
        if (child.grandchildren >= 2) grandchildren += child.grandchildren;
      }
      if(grandchildren >= 4){
        newCycle(id);
      }else{
        console.log("No new cycle for "+id)
      }
    }
  },
  err => {
    console.log(err);
  });
}

const newCycle = async (id) => {
  console.log("Adding new cycle for "+id);
  let parentID = -1;
  await db.one(id).then(result => {
    parentID = result.parentID;
    console.log("Getting parentID to check next cycle");
  },err=>{
    console.log(err);
  });
  await db.cycle(id,parentID).then(result => {
    if(parentID != -1){ 
      console.log("Will verify cycle for " + parentID);
      verifyCycle(parentID);// verify parent cycle
    }
  },err=>{
    console.log(err);
  });
}

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => `Server running on port ${PORT}`);