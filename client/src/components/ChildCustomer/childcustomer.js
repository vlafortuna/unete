import React, { Component } from 'react';
import {connect } from 'react-redux';
import {newCustomer} from '../../store/actions/customer'
import '../Customer/customers.css';
import usericon from '../../user.png';

export class ChildCustomerClass extends Component {
    constructor(props){
      super(props);
      this.state = {
        id: -1,
        name: '',
        parentID: -1,
        contribution: 0
      }
    }
  
    syncChanges(property, value, id){
      let state = {};
      state[property] = value;
      state['id'] = id;
      this.setState(state);
    }
  
    saveContributor(parentID){
      this.props.newCustomer(
        this.state.name,
        parentID,
        this.state.contribution
      ).then(result=>{
        this.props.getCustomers();

        let state = {};
        state.id = '-1';
        this.setState(state);
      });
    }
  
    render() {
      return this.props.customers.map((customer,index) => {
        if(Number(customer.parentID) === Number(this.props.parentID))
          return (
            <table>
              <tbody>
                <tr>
                  <td class={this.props.parentCycle === customer.parentCycle? 'isInParentCycle' : ''}>
                    <div class="customer"> 
                        <img class="usericon" alt="icon" src={usericon} /><br/> 
                        {customer.name} (${customer.contribution})<br/>
                        Cartera: ${customer.wallet} 
    
                        <form>
                          <h4>Nuevo colaborador</h4>
                          <div>
                            <input type="text" value={this.state.id === customer.id? this.state.name : ''} onChange={(ev)=>{ this.syncChanges('name',ev.target.value, customer.id)}} placeholder="Nombre"/>
                            <input type="number" value={this.state.id === customer.id? this.state.contribution : 0} onChange={(ev)=>{ this.syncChanges('contribution',ev.target.value, customer.id)}} placeholder="Contribución"/>
                            <button type="button" onClick={ ()=> this.saveContributor(customer.id) } >Insertar colaborador</button>
                          </div>
                        </form>
                    </div>
                  </td>
                </tr> 
                <tr>
                  <td>
                    <ChildCustomerClass key={customer.id} parentID={customer.id} parentCycle={customer.cycle} customers={this.props.customers} newCustomer={this.props.newCustomer} getCustomers={this.props.getCustomers}/>
                  </td>
                </tr>
              </tbody>
            </table>
          );
        else return false;
      });
    }
  };

  const mapStateToProps = (state) => ({
    customers: state.customers
  })
  
  const dispatchToProps = (dispatch) => ({
     newCustomer: (name,parentID,contribution) => dispatch(newCustomer(name,parentID,contribution))
  })
  
  export default connect(mapStateToProps, dispatchToProps)(ChildCustomerClass);