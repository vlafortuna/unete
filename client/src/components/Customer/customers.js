import React, { Component } from 'react';
import PropTypes from 'prop-types'
import {connect } from 'react-redux';
import {getCustomers,newCustomer} from '../../store/actions/customer'
import './customers.css';
import { ChildCustomerClass } from '../ChildCustomer/childcustomer';

class Customers extends Component {

  static propTypes = {
    getCustomers: PropTypes.func.isRequired,
    customers: PropTypes.array.isRequired
  }

  static defaultProps = {
    customers: []
  }

  componentWillMount() {
    this.props.getCustomers();
  }

  render() {
    return (
      <div id="canvan">
        <h2>Colaboradores</h2>
         <ChildCustomerClass key='0' parentID='-1' parentCycle="-1" customers={this.props.customers} newCustomer={this.props.newCustomer} getCustomers={this.props.getCustomers} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  customers: state.customers
})

const dispatchToProps = (dispatch) => ({
   getCustomers: () => dispatch(getCustomers()),
   newCustomer: (name,parentID,contribution) => dispatch(newCustomer(name,parentID,contribution))
})

export default connect(mapStateToProps, dispatchToProps)(Customers);
