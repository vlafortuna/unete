import React, { Component } from 'react'
import { Provider } from 'react-redux'
import logo from './logo.png'
import './App.css'
import store from './store'
import Customers from './components/Customer/customers'

class App extends Component {

  render () {
    return (
      <Provider store={ store }>
        <div className="App">
          <header className="App-header">
            <img src={ logo } className="App-logo" alt="logo"/>
          </header>
          <Customers id='-1'/>
        </div>
      </Provider>
    )
  }
}

export default App
