import {GET_CUSTOMERS, NEW_CUSTOMER} from './constants';

export const getCustomers = () => dispatch => {
  return fetch('/api/customers')
    .then(res => res.json())
    .then(customers => dispatch({type: GET_CUSTOMERS, payload: customers}))
}

// export const newCustomer = (name,parentID,contribution) => dispatch => {
//   return fetch('/api/newcustomer?name='+name+'&parentID='+parentID+'&contribution='+contribution)
//     .then(res => res.json())
//     .then(result => dispatch({type: NEW_CUSTOMER, payload: result}))
// }

export const newCustomer = (name,parentID,contribution) => dispatch => {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ "name":name,"parentID":parentID,"contribution":contribution })
  };
  return fetch('/api/newcustomer',requestOptions)
    .then(res => res.json())
    .then(result => dispatch({type: NEW_CUSTOMER, payload: result}))
}


