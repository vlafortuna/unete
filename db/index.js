const mysql = require('mysql');

const pool = mysql.createPool({
    connectionLimit: 10,
    host: "localhost",
    port: "3306",
    user: "root",
    password: "",
    database: "unete"
});

let db = {};

db.all = () => {
 return new Promise((resolve,reject) => {
    pool.query('select * from customers',(err,results) => {
        if(err){
            return reject(err);
        }
        return resolve(results);
    });
 });
};

db.one = (id) => {
    return new Promise((resolve,reject) => {
        pool.query('select C1.*, C2.id as childrenID, count(C3.id) as grandchildren from customers as C1 inner JOIN customers as C2 on C1.id = C2.parentID LEFT JOIN customers as C3 on C2.id = C3.parentID where C1.id = ? group by C2.id',[id],(err,results) => {
            if(err){
                return reject(err);
            }
            return resolve(results[0]);
        });
    });
};

db.children = (id) => {
    return new Promise((resolve,reject) => {
        pool.query('select C1.*, count(C2.id) as grandchildren from customers as C1 LEFT JOIN customers as C2 on (C1.id = C2.parentID and C1.cycle = C2.parentCycle) where C1.parentID = ? and C1.parentCycle = (select cycle from customers where id = ?) group by C1.id',[id,id],(err,results) => {
            if(err){
                return reject(err);
            }
            return resolve(results);
        });
    });
};

db.new = (name,parentID,contribution) => {
    return new Promise((resolve,reject) => {
        let cycle = Date.now(); // New timestamp cycle
        pool.query('insert into customers (id, parentID, name, contribution, cycle, parentCycle) VALUES (NULL, ?, ?, ?, ?,(select C.cycle from customers as C where C.id = ?))',[parentID,name,contribution,cycle,parentID],(err,results) => {
            if(err){
                return reject(err);
            }
            return resolve(results[0]);
        });
    });
};

db.cycle = (id,parentID) => {
    return new Promise((resolve,reject) => {
        let cycle = Date.now(); // New timestamp cycle
        pool.query('update customers set cycle = ?, parentCycle = (select C.cycle from customers as C where C.id = ?), wallet = wallet+(3*(select C.contribution from customers as C where C.id = ?)) where id = ?',[cycle, parentID, id, id],(err,results) => {
            if(err){
                return reject(err);
            }
            return resolve(true);
        });
    });
};

module.exports = db;